spytrek
=======

![spytrek.png](https://bitbucket.org/repo/gnkMnz/images/629266001-spytrek.png)

A game that runs in the terminal, written with python 2.7 and the curses library.

Check out the game from git to play

    git clone git@bitbucket.org:jconkey/spy-trek.git
    cd spytrek/bin

You can play it by yourself by running it with no options:

    ./spytrek.py

You can also play it over the network in two-player mode:

    Server player:
        ./spytrek.py -s
    
    Second player:
        ./spytrek.py -c the.server.ip.address

Keys
   * Move: arrow keys or WASD
   * Small missile: Space
   * Large missile: M
   * Quit: Q


Credits

**Jason Conkey**
   * concept, animation, event model

**St. John Johnson**
   * two player mode, initial framework

License

WTFPL - http://www.wtfpl.net/