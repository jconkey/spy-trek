#!/opt/local/bin/python2.7

import curses
import math
import time


stdscr = curses.initscr()
curses.noecho()
curses.raw()
curses.cbreak()
stdscr.nodelay(1)

# hide cursor
try:
    curses.curs_set(0) 
except curses.error:
    pass

try:
    curses.start_color()
except curses.error:
    print "error"
    pass

stdscr.addstr(0, 0, "Color table", curses.A_REVERSE)
stdscr.refresh()
stdscr.keypad(1)

boardWidth = 100
boardHeight = 256
screen = curses.newwin(boardHeight+1, boardWidth, 5, 10)


def initRGBColor(code, r, g, b):
    r = int(math.floor(r/0.255))
    g = int(math.floor(g/0.255))
    b = int(math.floor(b/0.255))
    curses.init_color(code, r, g, b)

stdscr.bkgd(' ', curses.color_pair(0) | curses.A_BOLD)
screen.bkgd(' ', curses.color_pair(1))

for color_pair in range(1, 255):
    curses.init_pair(color_pair, 0, color_pair)


while 1:
    color_pair = 0
    screen.clear()
    
    for column in range(0, 8):
        row = 0
        for color in range(column*32, column*32+32):
            screen.addstr(row, 9*column, str(color)+':    ', curses.color_pair(color))
            row += 1
    
    screen.refresh()
    c = -1
    while True:
        nextc = stdscr.getch()
        if nextc == -1:
            break
        c = nextc
    if c == ord('q'):
        break  # Exit the while()
    time.sleep(0.25)

curses.endwin()
