#!/opt/local/bin/python2.7
# -*- coding: UTF-8 -*-

import curses
import random
from collections import deque, defaultdict
import socket
import sys
import getopt
import time
import math
import json
import logging
import spytrek


class UsageException(Exception):
    '''raise to show the usage help'''

def usage():
    print "spytrek [options]"
    print "    options:"
    print "        -d: debug (draw one frame per key press)"
    print "        -s: run as the server"
    print "        -c the.server.ip.address: run as the client and connect to the server at the given IP address"


class Events(object):
    '''Handle events that must be transferred between running instances
        Provides a queue of events that are sent/recd each frame.'''
    
    def __init__(self):
        self.event_list = []
    
    def add(self, type, name, params):
        self.event_list.append([type, name, params])
    
    def get(self):
        return self.event_list

    def clear(self):
        self.event_list = []


class Sprites(object):
    '''A collection of sprites'''
    
    # unique id generation
    id = 0
    @classmethod
    def new_id(cls):
        cls.id += 1
        return cls.id
        
    def __init__(self, screen, colors, events):
        self.screen = screen
        self.colors = colors
        self.events = events
        self.sprites = {}
    
    def event(self, type, name, params):
        self.events.add(type, name, params)
    
    def add(self, sprite):
        self.sprites[sprite.id] = sprite
        
    def remove(self, sprite, make_event=True):
        '''remove the given sprite. Makes an event by default'''
        if sprite.id in self.sprites:
            del self.sprites[sprite.id]
            if make_event:
                self.event(sprite.__class__.__name__, 'remove', {'id': sprite.id})
    
    def remove_by_id(self, id):
        '''remove a sprite by its id. Does not make an event'''
        if id in self.sprites:
            del self.sprites[id]
    
    def process(self, distance, users):
        # using values() instead of itervalues() because
        # we may add to/remove from sprites dict during loop
        logging.debug(self.sprites.keys())
        for sprite in self.sprites.values():
            if sprite.finished():
                self.remove(sprite)
            else:   
                sprite.process(self.screen, distance, users)
    
    def each(self, type, callback, *args):
        '''call the given callback function for each sprite of the given type
            optionally pass any number of arguments that will be passed to 
            callback function'''
        for sprite in self.sprites.values():
            if isinstance(sprite, type):
                callback(sprite, *args)
    
    def add_ch_trans(self, x, y, char, foreground):
        self.screen.add_panel_ch_trans(x, y, char, foreground, self.colors)


class Sprite(object):
    '''
    Base class for a game element that can be bigger than one character,
    and has position, state, animation, etc.
    '''
    
    def __init__(self, container, x, y, frame, frames, life=-1, id=None):
        self.container = container
        if id == None:
            self.id = Sprites.new_id()
        else:
            self.id = id
        self.x = x
        self.y = y
        self.frame = frame
        self.frames = frames
        self.life = life
        if container:
            container.add(self)
    
    def rotate_frame(self):
        self.frame += 1;
        if self.frame >= len(self.frames):
            self.frame = 0
    
    def __del__(self):
        pass
    
    def collisions(self, screen, distance, users):
        pass
    
    def process(self, screen, distance, users):
        self.render(screen, distance, users)
        self.collisions(screen, distance, users)
        self.step(screen, distance, users)
    
    def event(self, name, params):
        self.container.event(self.__class__.__name__, name, params)

class Bird(Sprite):
    def __init__(self, container, x, y, dx, dy, id=None, make_event=True):
        super(Bird, self).__init__(
            container,
            x, 
            y,
            random.randint(0, 2),
            [0, 0, 0, 0, 1, 1, 0, 2, 2],
            id=id
        )
        self.frames = [
            '-=-',
            '\=/',
            '/=\\',
        ]
        self.dx = dx
        self.dy = dy
        if make_event:
            self.event('add', {
                'x': x, 'y': y, 
                'dx': dx, 'dy': dy, 
                'id': self.id
            })
    
    def finished(self):
        if self.y > self.container.screen.PANEL_H or self.x < -5 or self.x > self.container.screen.PANEL_W + 3:
            return True
        return False
    
    def render(self, screen, distance, users):
        y, x = int(self.y), int(self.x)
        delta_x = 0
        for char in self.frames[self.frame]:
            self.container.add_ch_trans(x+delta_x, y, char, 'white')
            delta_x += 1
    
    def step(self, screen, distance, users):
        self.rotate_frame()
        self.x += self.dx
        self.y += self.dy
            

class Boulder(Sprite):
    def __init__(self, container, x, y, dx, dy, life, type=0, id=None, make_event=True):
        super(Boulder, self).__init__(
            container,
            x, 
            y,
            0,
            [0, 1, 1, 2, 1, 1],
            id=id
        )
        self.dx = dx
        self.dy = dy
        self.life = life
        self.state = 0 # 0: active, 1: exploding (then removed)
        self.colors = [self.container.colors.named('boulder'), self.container.colors.named('boulder_exploding')]
        self.type = type # 0: regular boulder, 1: shard
        if self.type == 0:
            self.width = 8
            self.height = 5
            self.frames = [
                [
                    ' .----. ',
                    '///////\\',
                    '|//##//|',
                    '\\///////',
                    ' .----. ',
                ],
                [
                    ' .----. ',
                    '/======\\',
                    '|==##==|',
                    '\\======/',
                    ' .----. ',
                ],
                [
                    ' .----. ',
                    '/\\\\\\\\\\\\\\',
                    '|\\\\##\\\\|',
                    '\\\\\\\\\\\\\\/',
                    ' .----. ',
                ],
            ]
        else:
            self.width = 4
            self.height = 3
            self.frames = [
                [
                    ' .-.',
                    '|//#',
                    ' .-.',
                ],
                [
                    '  __  ',
                    '/====\\',
                    '|=##=|',
                ],
                [
                    '---. ',
                    '#\\\\|',
                    '---. ',
                ],
            ]
        if make_event:
            self.event('add', {
                'x': x, 'y': y, 
                'dx': dx, 'dy': dy, 
                'life': life, 'type': type, 
                'id': self.id
            })
    
    def finished(self):
        return self.life == 0 or not (
            -self.height < self.y < self.container.screen.PANEL_H + self.height
            or -self.width < self.x < self.container.screen.PANEL_W + self.width
        )
    
    def render(self, screen, distance, users):
        x, y = int(self.x), int(self.y)
        frame = self.frames[self.frame]
        for delta_y in range(0, self.height):
            delta_x = 0
            for char in frame[delta_y]:
                if char != ' ':
                    screen.add_panel_str(x+delta_x, y+delta_y, char, self.colors[self.state])
                delta_x += 1
    
    def point_collision(self, x, y):
        frame = self.frames[self.frame]
        sx, sy = int(self.x), int(self.y)
        if sx <= x < sx + self.width and sy <= y < sy + self.height:
            frame_y = int(y-sy)
            frame_x = int(x-sx)
            if frame[frame_y][frame_x] != ' ':
                return True
        return False
    
    def collisions(self, screen, distance, users):
        user = users[spytrek.user_id]
        if not user['crashed']:
            # user hit boulder?
            if self.point_collision(user['x'], user['y']):
                user['crashed'] = True
                self.container.events.add('User', 'crash', {'id': spytrek.user_id})

    def step(self, screen, distance, users):
        self.rotate_frame()
        self.y += self.dy
        self.x += self.dx
        self.life -= 1


class Rocket(Sprite):
    
    # track number of currently instantiated rockets for each player
    num_rockets = False
    
    @classmethod
    def rocket_count(cls, type, user_id):
        return cls.num_rockets[type][user_id]
    
    @classmethod
    def add_rocket(cls, type, user_id):
        cls.num_rockets[type][user_id] += 1
    
    @classmethod
    def remove_rocket(cls, type, user_id):
        cls.num_rockets[type][user_id] -= 1
    
    def __init__(self, container, x, y, dx, dy, life=1, type=0, id=None, make_event=True):
        super(Rocket, self).__init__(
            container,
            x, 
            y,
            0,
            [0, 1, 2, 3],
            id=id
        )
        self.dx = dx
        self.dy = dy
        self.life = life
        self.state = 0 # 0: active, 1: exploding (then removed)
        self.colors = {
            '^': 'white',
            '|': 'white',
            '/': 'white',
            '\\': 'white',
            '$': 'red',
            '*': 'orange',
            '#': 'orange',
            '%': 'yellow',
            '+': 'blue',
            '.': 'red',
            'o': 'yellow',
        }
        self.type = type # 0: regular rocket, 1: super rocket
        if self.type == 0:
            self.width = 3
            self.height = 3
            self.frames = [
                [
                    ' o ',
                    ' | ',
                    '/$\\',
                ],
                [
                    ' o ',
                    ' | ',
                    '/.\\',
                ],
                [
                    ' o ',
                    ' | ',
                    '/$\\',
                ],
            ]
        else:
            self.width = 5
            self.height = 7
            self.frames = [
                [
                    ' /+\\ ',
                    '|+++|',
                    ' \\+/ ',
                    ' |$| ',
                    ' |$| ',
                    ' /#\\ ',
                    '/###\\',
                ],
                [
                    ' /+\\ ',
                    '|+++|',
                    ' \\+/ ',
                    ' |#| ',
                    ' |$| ',
                    ' /$\\ ',
                    '/###\\',
                ],
                [
                    ' /+\\ ',
                    '|+++|',
                    ' \\+/ ',
                    ' |#| ',
                    ' |#| ',
                    ' /$\\ ',
                    '/$$$\\',
                ],
                [
                    ' /+\\ ',
                    '|+++|',
                    ' \\+/ ',
                    ' |$| ',
                    ' |#| ',
                    ' /#\\ ',
                    '/$$$\\',
                ],
            ]
        self.add_rocket(self.type, spytrek.user_id)
        if make_event:
            self.event('add', {
                'x': x, 'y': y, 
                'dx': dx, 'dy': dy, 
                'life': life,
                'type': type, 
                'id': self.id
            })
    
    def __del__(self):
        self.remove_rocket(self.type, spytrek.user_id)

    def finished(self):
        
        return self.life <= 0 or not (
            -self.height < self.y < self.container.screen.PANEL_H + self.height
            or -self.width < self.x < self.container.screen.PANEL_W + self.width
        )
    
    def render(self, screen, distance, users):
        y, x = int(self.y), int(self.x)
        frame = self.frames[self.frame]
        for delta_y in range(0, self.height):
            delta_x = 0
            for char in frame[delta_y]:
                if char != ' ':
                    self.container.add_ch_trans(x+delta_x, y+delta_y, char, self.colors[char])
                delta_x += 1
    
    def boulder_collision(self, boulder):
        if boulder.point_collision(self.x, self.y):
            self.life = 0
            if boulder.type == 0:
                Boulder(self.container, boulder.x, boulder.y, -1, 1, 8, 1)
                Boulder(self.container, boulder.x+4, boulder.y, 1, 1, 8, 1)
            boulder.state = 1
            boulder.life = 2
            return False
    
    def collisions(self, screen, distance, users):
        user = users[spytrek.user_id]
        if not user['crashed']:
            # rocket tip hit user?
            if self.x+1 == user['x'] and self.y == user['y']:
                self.life = 0
                user['crashed'] = True
                self.container.events.add('User', 'crash', {'id': spytrek.user_id})

        # if rocket hits a boulder then remove rocket, set boulder to
        # exploding state, and add two boulder shards
        self.container.each(Boulder, self.boulder_collision)

    def step(self, screen, distance, users):
        self.rotate_frame()
        self.y += self.dy
        self.x += self.dx

Rocket.num_rockets = defaultdict(lambda: defaultdict(int))

def make_row(screen, road, distance, sprites):
    row = []
    for x in range(0, road['left']-2):
        row.append('x')
    for x in range(road['left']-2, road['left']):
        row.append('#')
    for x in range(road['left'], road['right']):
        row.append(' ')
    for x in range(road['right'], road['right']+2):
        row.append('#')
    for x in range(road['right']+2, screen.PANEL_W):
        row.append('x')
    
    if spytrek.game_mode == 0 or spytrek.game_mode == 1:
        # add a bird?
        if 0 == random.randint(0, 15):
            if 0 == random.randint(0, 1):
                Bird(sprites, 1, random.randint(1, 5), 0.25*random.randint(2, 4), 0.25*random.randint(1, 5))
            else:
                Bird(sprites, screen.PANEL_W-1, random.randint(1, 5), -0.25*random.randint(2, 4), 0.25*random.randint(1, 5))
    
        if distance[spytrek.user_id] > 0:
            #replace rocks with Rock sprites dropped by birds
            # add a big or little rock?
            #if 0 == random.randint(0, 15):
                #offset = random.randint(0, int((road['right']-road['left'])/2))
                #row[road['left'] + offset] = '*'
                #if 0 == random.randint(0, 5):
                #    row[road['left'] + offset - 1] = '*'
                #    row[road['left'] + offset + 1] = '*'
        
            # add a boulder?
            if (0 == random.randint(0, 20)):
                dx = 0.01 + 0.25*random.randint(0, 4)
                life = (road['right']-road['left']+10)/dx
                if 0 == random.randint(0, 1):
                    Boulder(sprites, road['left']-9, -6, dx, 1, life, random.randint(0, 1))
                else:
                    Boulder(sprites, road['right']+1, -6, -dx, 1, life, random.randint(0, 1))
    
    return row


def scroll(board, newRow, users):
    board.pop()
    board.appendleft(newRow)

    for index, user in enumerate(users):
        if user['crashed']:
            user['y'] += 1


def draw(screen, board, users, distance, speed, sprites, colors):
    screen.clear_panel()

    for y in range(0, screen.PANEL_H):
        row = board[y]
        for x in range(0, screen.PANEL_W):
            cell = row[x]
            if cell == 'x':
                screen.add_panel_str(x, y, ' ', colors.named('land'))
            elif cell == '#':
                screen.add_panel_str(x, y, '#', colors.named('shoulder'))
            else:
                screen.add_panel_str(x, y, cell, colors.named('road'))

    for index, user in enumerate(users):
        if user['crashed']:
            explSizeY = int(math.ceil(user['explMaxHeight'] * user['explProgress']))
            explSizeX = int(math.ceil(user['explMaxWidth'] * user['explProgress']))
            for y in range(-explSizeY, explSizeY):
                for x in range(-explSizeX, explSizeX):
                    if (y < (-explSizeY+1) or y >= (explSizeY-1)) and (x < (-explSizeX+1) or x >= (explSizeX-1)):
                        continue
                    screen.add_panel_str(
                        user['x'] + x,
                        user['y'] + y,
                        '@-+*#.$%!'[random.randint(0, 8)],
                        colors.named('crash' + str(random.randint(0,2))))
            if user['explState'] == 2:
                user['explProgress'] += user['explProgressIncrement']
                if user['explProgress'] >= 1:
                    user['explProgress'] = 1
                    user['explState'] = 1
            elif user['explState'] == 1:
                user['explProgress'] -= user['explProgressIncrement']
                if user['explProgress'] <= 0.25:
                    user['explState'] = 0
        else:
            screen.add_panel_str(user['x'], user['y'], '@', colors.named('user' + str(index)))

    sprites.process(max(distance[0], distance[1]), users)
    
    if users[spytrek.user_id]['crashed']:
        message = 'YOU CRASHED! final score: ' + str(distance[spytrek.user_id])
        screen.add_panel_str(int(screen.PANEL_W / 2), int((screen.PANEL_H - len(message)) / 2), message)
    else:
        screen.add_panel_str(0, 0, 'distance: ' + str(distance[spytrek.user_id]))
        screen.add_panel_str(0, 1, 'speed:' + str(int(1.0/speed['vel'])))
    
#    screen.add_panel_str(0, 2, 'user ' + str(0) +': crashed='+str(users[0]['crashed']))
#    screen.add_panel_str(0, 3, 'user ' + str(1) +': crashed='+str(users[1]['crashed']))

    screen.refresh_panel()


def vary_road(road, distance, events):
    # are we going to change the trending direction?
    if random.randint(0,9) == 0:
        if road['trend'] == -1:
            road['trend'] = 0
        elif road['trend'] == 0:
            if random.randint(0,1):
                road['trend'] = -1
            else:
                road['trend'] = 1
        else:
            road['trend'] = 0

    # move in the trending direction
    if road['trend'] == -1 and road['left'] > 4:
        road['left'] -= 1
        road['right'] -= 1
    elif road['trend'] == 1 and road['right'] < screen.PANEL_W-4:
        road['left'] += 1
        road['right'] += 1

    # shrink road periodically
    if 1 == max(distance[0], distance[1]) % 40 and road['right']-road['left'] > 1:
        if random.randint(0,1):
            road['left'] += 1
        else:
            road['right'] -= 1
    
    events.add('Road', 'vary', {
        'trend': road['trend'],
        'left': road['left'],
        'right': road['right'],
    })


def user_road_crash(board, user):
    cell = board[user['y']][user['x']]
    if cell == 'x':
        return True
    return False

def user_user_crash(users, current_user_id):
    userx = users[current_user_id]['x']
    usery = users[current_user_id]['y']
    for i, user in enumerate(users):
        if i != current_user_id and userx == user['x'] and usery == user['y']:
            return True
    return False


def init_board(screen, road, distance, sprites):
    board = deque()
    for y in xrange(0, screen.PANEL_H):
        board.appendleft(make_row(screen, road, distance, sprites))
    return [board, road, distance]


def process_events(users, sprites, road, event_list):
    logging.debug(event_list)

    # event format is [event type, params dict]
    for event in event_list:
        type = event[0]
        name = event[1]
        params = event[2]
        if 'User' == type:
            other_user = users[params['id']]
            if 'move' == name:
                other_user['x'] += params['dx']
                other_user['y'] += params['dy']
            elif 'crash' == name:
                other_user['crashed'] = True
        elif 'Bird' == type:
            if 'add' == name:
                Bird(
                    sprites,
                    params['x'], params['y'],
                    params['dx'], params['dy'],
                    id=params['id'],
                    make_event=False)
            elif 'remove' == name:
                sprites.remove_by_id(params['id'])
        elif 'Boulder' == type:
            if 'add' == name:
                Boulder(
                    sprites,
                    params['x'], params['y'],
                    params['dx'], params['dy'],
                    params['life'], params['type'],
                    id=params['id'],
                    make_event=False)
            elif 'remove' == name:
                sprites.remove_by_id(params['id'])
        elif 'Rocket' == type:
            if 'add' == name:
                Rocket(
                    sprites,
                    params['x'], params['y'],
                    params['dx'], params['dy'],
                    params['life'], params['type'],
                    id=params['id'],
                    make_event=False)
            elif 'remove' == name:
                sprites.remove_by_id(params['id'])
        elif 'Road' == type:
            if 'vary' == name:
                road['trend'] = params['trend']
                road['left'] = params['left']
                road['right'] = params['right']


def main(argv, screen):
    try:
        opts, args = getopt.getopt(argv, "dsc:", [])
        opts = dict(opts)
        keys = opts.keys()
    except getopt.GetoptError:
        raise UsageException()
    
    BOARD_MAX_X, BOARD_MAX_Y = 100, 25
    screen.init_panel(BOARD_MAX_X, BOARD_MAX_Y)
    screen.nodelay('-d' not in keys)
    
    KEYS_UP = [curses.KEY_UP, 'w']
    KEYS_DOWN = [curses.KEY_DOWN, 's']
    KEYS_LEFT = [curses.KEY_LEFT, 'a']
    KEYS_RIGHT = [curses.KEY_RIGHT, 'd']
    KEYS_ROCKET = [ord(' ')]
    KEYS_ROCKET_BIG = [ord('m')]
    KEYS_VERT = KEYS_UP + KEYS_DOWN
    KEYS_HORIZ = KEYS_LEFT + KEYS_RIGHT
    KEYS = KEYS_VERT + KEYS_HORIZ
    
    # hide cursor
    try:
        curses.curs_set(0)
    except curses.error:
        pass
    
    colors = spytrek.Colors()
    colors.define('white', 231)
    colors.define('brown', 94)
    colors.define('lightbrown', 130)
    colors.define('green', 64)
    colors.define('lightgreen', 76)
    colors.define('blue', 19)
    colors.define('lightblue', 33)
    colors.define('red', 1)
    colors.define('lightred', 202)
    colors.define('lightgray', 248)
    colors.define('black', 232)
    colors.define('gray', 244)
    colors.define('yellow', 11)
    colors.define('orange', 208)
    colors.define('darkpurple', 88)
    colors.define('darkgray', 239)
    
    # this white-black color must be defined first
    # because it is the default text color
    colors.add('white', 'black', 'score'),
    
    colors.add('darkpurple', 'darkgray', 'boulder'),
    colors.add('yellow', 'red', 'boulder_exploding'),
    
    colors.add('red', 'lightred', 'user0'),
    colors.add('blue', 'lightblue', 'user1'),
    
    colors.add('white', 'green', 'land'),
    
    colors.add('black', 'brown', 'road'),
    colors.add('gray', 'brown', 'shoulder'),
    
    colors.add('white', 'red', 'crash0'),
    colors.add('black', 'yellow', 'crash1'),
    colors.add('gray', 'orange', 'crash2'),
    
    colors.add_transparent('white')
    colors.add_transparent('black')
    colors.add_transparent('red')
    colors.add_transparent('blue')
    colors.add_transparent('orange')
    colors.add_transparent('yellow')
    logging.debug(colors.get_color_pairs())
    
    screen.add_window_str(10, 2, "Spy Trek", curses.A_REVERSE)
    screen.refresh_window()
    
    conn = spytrek.Connection()
    
    if '-s' in keys:
        screen.write_centered("Waiting for connection...")
        conn.server()
        spytrek.user_id = 0
        spytrek.game_mode = 1
    elif '-c' in keys:
        screen.write_centered("Connecting to %s..." % (opts['-c']))
        conn.client(opts['-c'])
        spytrek.user_id = 1
        spytrek.game_mode = 2
    else:
        spytrek.user_id = 0
        spytrek.game_mode = 0
    
    speed = {
        'vel': 0.15,
        'acc': 0.90
    }
    
    # single-player game by default. In 2-player mode,
    # user 0 is the server and user 1 is the client
    users = [{
        'x': 50,
        'y': 20,
        'crashed': False,

        'explMaxWidth': 9,
        'explMaxHeight': 6,
        'explState': 2,
        'explProgress': 0.25,
        'explProgressIncrement': 0.25,
    }]
    if spytrek.game_mode == 1 or spytrek.game_mode == 2:
        users.append(users[0].copy())
        users[0]['x'] -= 1
        users[1]['x'] += 1
    
    road = {
        'trend': 0,
        'left': 35,
        'right': 65,
    }
    
    events = Events()
    
    sprites = Sprites(screen, colors, events)
    
    distance = [0, 0]
    board, road, distance = init_board(screen, road, distance, sprites)
    draw(screen, board, users, distance, speed, sprites, colors)
    
    logging.debug('user_id: '+str(spytrek.user_id))
    
    while True:
        c = -1;
        if '-d' in keys:
            # in debug mode, go one frame for each keypress
            c = screen.get_key()
        else:
            while True:
                nextc = screen.get_key()
                if nextc == -1:
                    break
                c = nextc
        if c == ord('q'):
            events.add('User', 'quit', {'id': spytrek.user_id})
            break  # Exit the while()
        elif not users[spytrek.user_id]['crashed']:
            if c in KEYS_DOWN:
                users[spytrek.user_id]['y'] = min(24, users[spytrek.user_id]['y'] + 1)
                events.add('User', 'move', {'id': spytrek.user_id, 'dx': 0, 'dy': 1})
            elif c in KEYS_UP:
                users[spytrek.user_id]['y'] = max(0, users[spytrek.user_id]['y'] - 1)
                events.add('User', 'move', {'id': spytrek.user_id, 'dx': 0, 'dy': -1})
            elif c in KEYS_RIGHT:
                users[spytrek.user_id]['x'] = min(99, users[spytrek.user_id]['x'] + 1)
                events.add('User', 'move', {'id': spytrek.user_id, 'dx': 1, 'dy': 0})
            elif c in KEYS_LEFT:
                users[spytrek.user_id]['x'] = max(0, users[spytrek.user_id]['x'] - 1)
                events.add('User', 'move', {'id': spytrek.user_id, 'dx': -1, 'dy': 0})
            elif c in KEYS_ROCKET:
                if True or Rocket.rocket_count(0, spytrek.user_id) < 2:
                    r = Rocket(sprites, users[spytrek.user_id]['x'], users[spytrek.user_id]['y']-3, 0, -0.5)
                    logging.debug('ADD rocket: '+str(r.id))
            elif c in KEYS_ROCKET_BIG:
                r = Rocket(sprites, users[spytrek.user_id]['x'], users[spytrek.user_id]['y']-3, 0, -0.5, type=1)
                logging.debug('ADD BIG rocket: '+str(r.id))
        
        # handle collisions
        if not users[spytrek.user_id]['crashed']:
            # detect user to [user, road]
            # (user to Sprites collisions is checked in Sprite collisions handler)
            if user_user_crash(users, spytrek.user_id) or user_road_crash(board, users[spytrek.user_id]):
                users[spytrek.user_id]['crashed'] = True
                events.add('User', 'crash', {'id': spytrek.user_id})
        
        # send events from this event loop
        if spytrek.game_mode == 0:
            logging.debug(events.get())
            events.clear()
        elif spytrek.game_mode == 1:
            conn.send(json.dumps(events.get()))
            events.clear()
            process_events(users, sprites, road, json.loads(conn.recv()))
        elif spytrek.game_mode == 2:
            process_events(users, sprites, road, json.loads(conn.recv()))
            conn.send(json.dumps(events.get()))
            events.clear()
        
        draw(screen, board, users, distance, speed, sprites, colors)
        
        if spytrek.game_mode > 0:
            # if any player has not crashed, all players scroll
            c0 = users[spytrek.user_id]['crashed']
            c1 = users[1-spytrek.user_id]['crashed']
            if not c0:
                distance[spytrek.user_id] += 1
            if not c1:
                distance[1-spytrek.user_id] += 1
            if not c0 or not c1:   
                scroll(board, make_row(screen, road, distance, sprites), users)
        else:
            if not users[spytrek.user_id]['crashed']:
                distance[spytrek.user_id] += 1
                scroll(board, make_row(screen, road, distance, sprites), users)
        
        if spytrek.game_mode != 2:
            # the server generates the road
            vary_road(road, distance, events)
        
        if 0 == max(distance[0], distance[1]) % 40:
            speed['vel'] *= speed['acc']
        
        time.sleep(max(speed['vel'], 0.04))

if (__name__ == '__main__'):
    logging.basicConfig(filename='spytrek.log', level=logging.DEBUG)
    
    exit_code = 0
    screen = None
    
    try:
        screen = spytrek.Screen()
        exit_code = main(sys.argv[1:], screen)
        screen.close()
    except UsageException:
        if screen: screen.close()
        usage()
    except Exception, e:
        logging.debug(e)
        if screen: screen.close()
        raise
        
    sys.exit(exit_code)
