import socket
#import logging

#logging.basicConfig(filename='connection.log',level=logging.ERROR)

class Connection(object):
    """Basic Socket Abstraction"""

    """Constructor with port settings"""
    def __init__(self, port=12345):
        super(Connection, self).__init__()
        self.port = port

    """Destructor for cleaning up sockets"""
    def __del__(self):
        if hasattr(self, 'conn'):
            self.conn.close()
        if hasattr(self, 'sock'):
            self.sock.close()

    def server(self):
        self.sock = socket.socket()
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(('', self.port));
        self.sock.listen(5)
        self.conn, self.addr = self.sock.accept()

    def client(self, host='127.0.0.1'):
        self.conn = socket.socket()
        self.conn.connect((host, self.port))

    def send(self, data):
#        logging.debug('send:' + data)
        self.conn.sendall(data)

    def recv(self):
        data = self.conn.recv(2024)
#        logging.debug('recv:' + data)
        return data