import curses


class ScreenError(EnvironmentError):
    """Screen Error"""
    pass


class Screen(object):
    """Class to manage interaction with curses"""

    def __init__(self):
        super(Screen, self).__init__()
        
        # Create overall window
        self.init_window()

        # Setup curses settings
        curses.noecho()
        curses.raw()
        curses.cbreak()

    def init_window(self):
        # Create overall widow
        self.window = curses.initscr()
        self.window.keypad(1)
        curses.start_color()
        self.window.bkgd(' ', curses.color_pair(0) | curses.A_BOLD)

        # Create panel
        self.WINDOW_H, self.WINDOW_W = self.window.getmaxyx()

    def init_panel(self, width, height):
        self.PANEL_W = width
        self.PANEL_H = height

        # Create panel centered in the window
        self.panel = curses.newwin(
            self.PANEL_H,
            self.PANEL_W,
            int((self.WINDOW_H - self.PANEL_H) / 2),
            int((self.WINDOW_W - self.PANEL_W) / 2)
        )

        # writing to last row, last col won't cause an exception.
        # without this, curses tries to move the cursor to the next
        # position which doesn't exist
        self.panel.leaveok(1)

        self.panel.bkgd(' ', curses.color_pair(1))
    
    def nodelay(self, nodelay=1):
        self.window.nodelay(nodelay)

    def clear_panel(self):
        self.panel.clear()

    def clear_window(self):
        self.window.clear()

    def refresh_panel(self):
        self.panel.refresh()

    def refresh_window(self):
        self.window.refresh()

    def get_center_x(self, message):
        return int((self.WINDOW_W - len(message)) / 2)

    def get_center_y(self):
        return int(self.WINDOW_H / 2)
    
    def write_centered(self, message, attr=curses.A_REVERSE, refresh=True):
        self.add_window_str(self.get_center_x(message), self.get_center_y(), message, curses.A_REVERSE)
        if refresh:
            self.refresh_window()

    def add_panel_str(self, x, y, string, attr=curses.A_NORMAL):
        if (0 <= y < self.PANEL_H) and (0 <= x < self.PANEL_W):
            return self._add_str(self.panel, x, y, string, attr)

    def add_window_str(self, x, y, string, attr=curses.A_NORMAL):
        if (0 <= y < self.WINDOW_H) and (0 <= x < self.WINDOW_W):
            return self._add_str(self.window, x, y, string, attr)

    def _add_str(self, panel, x, y, string, attr=curses.A_NORMAL):
        # writing to last position in buffer draws the character
        # but causes an exception when the cursor is moved to the 
        # next position outside the buffer. Writing outside the
        # bounds of the buffer also causes an exception
        try:
            panel.addstr(y, x, string, attr)
        except Exception:
            pass
    
    def add_panel_ch_trans(self, x, y, char, foreground, colors):
        '''draw character on the game panel, and get background color 
            under the char location and use it for the background 
            color of the char, so the char will have a transparent 
            background'''
        # 1. don't write outside the buffer
        # 2. catch exception caused by writing to last position
        #    in buffer when cursor moves off the screen
        if (0 <= y < self.PANEL_H) and (0 <= x < self.PANEL_W):
            ch = self.panel.inch(y, x)
            attr = ch - (ch & 255)
            color_pair = colors.color_with_same_background(foreground, attr)
            try:
                self.panel.addch(y, x, char, color_pair)
            except Exception, e:
                pass
    
    def get_key(self):
        return self.window.getch()

    def close(self):
        self.refresh_window()
        curses.endwin()
