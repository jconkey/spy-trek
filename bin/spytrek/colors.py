from collections import defaultdict
import curses

class Colors(object):
    '''color management class for curses'''

    def __init__(self):
        self.color_codes = {}
        self.color_pair_number = 0
        self.color_pairs = defaultdict(dict) # auto-vivification for [x][y]
        self.color_pair_names = {}
    
    def define(self, name, code):
        '''define a color code by name'''
        self.color_codes[name] = code
    
    def name_to_code(self, name_or_code):
        if isinstance(name_or_code, int):
            return name_or_code
        return self.color_codes[name_or_code]
    
    def add(self, foreground, background, pair_name = None):
        '''make a color pair by color names, optionally named.
            Only creates a new color pair if not already created'''
        foreground = self.name_to_code(foreground)
        background = self.name_to_code(background)
        if not (background in self.color_pairs and foreground in self.color_pairs[background]):
            self.color_pair_number += 1
            curses.init_pair(self.color_pair_number, foreground, background)
            self.color_pairs[background][foreground] = curses.color_pair(self.color_pair_number)
        if pair_name != None:
            self.color_pair_names[pair_name] = self.color_pairs[background][foreground]
    
    def add_transparent(self, foreground, backgrounds = None):
        '''this should be used after all regular colors have been
            added so that the desired backgrounds are defined, else
            specify the specific background colors needed for this
            foreground color'''
        foreground = self.name_to_code(foreground)
        if None == backgrounds:
            backgrounds=self.color_pairs.keys()
        for background in backgrounds:
            self.add(foreground, background)
    
    def named(self, pair_name):
        '''return a color pair number by pair name'''
        return self.color_pair_names[pair_name]
    
    def pair(self, foreground, background):
        '''return a color pair number by foreground and background color names'''
        return self.color_pairs[self.name_to_code(background)][self.name_to_code(foreground)]
    
    def color_with_same_background(self, foreground, attr):
        '''given foreground color name and attr from a position on the screen,
            return the color pair with the given foreground color where the 
            background color matches the background in attr.'''
        under_color_pair_number = curses.pair_number(attr)
        under_foreground, under_background = curses.pair_content(under_color_pair_number)
        return self.color_pairs[under_background][self.name_to_code(foreground)]
    
    def get_color_pairs(self):
        return self.color_pairs
